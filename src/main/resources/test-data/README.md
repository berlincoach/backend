For each entity set, test data can optionally be provided in a file *EntitySetName.json*.

Test data is available only in "test mode", not in production mode.

Please refer to the generated TestSettings class to see the options for enabling test mode.

Sample test data for file AddressSet.json:

''' JSON
[
    {
        "AddressID": "101",
        "Country": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "HouseNumber": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "PostalCode": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "Street": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "Town": "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    },
    {
        "AddressID": "102",
        "Country": "XYZ",
        "HouseNumber": "XYZ",
        "PostalCode": "XYZ",
        "Street": "XYZ",
        "Town": "XYZ"
    }
]
'''

Sample test data for file CustomerSet.json:

''' JSON
[
    {
        "AddressID": "101",
        "CompanyName": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "CustomerID": "201",
        "Email": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "Name": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "Phone": "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    },
    {
        "AddressID": "102",
        "CompanyName": "XYZ",
        "CustomerID": "202",
        "Email": "XYZ",
        "Name": "XYZ",
        "Phone": "XYZ"
    }
]
'''

Sample test data for file JobSet.json:

''' JSON
[
    {
        "ActualWorkHours": 32767,
        "DoneDate": "2019-11-05T08:52:51.136Z",
        "JobID": "301",
        "JobStatusID": "1234567890123",
        "PredictedWorkHours": 32767,
        "Suggested": false,
        "TaskID": "1201",
        "Title": "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    },
    {
        "ActualWorkHours": null,
        "DoneDate": null,
        "JobID": "302",
        "JobStatusID": "9876543210987",
        "PredictedWorkHours": 0,
        "Suggested": true,
        "TaskID": "1202",
        "Title": "XYZ"
    }
]
'''

Sample test data for file MachineSet.json:

''' JSON
[
    {
        "MachineID": "401",
        "Name": "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    },
    {
        "MachineID": "402",
        "Name": "XYZ"
    }
]
'''

Sample test data for file MaterialPositionSet.json:

''' JSON
[
    {
        "JobID": "301",
        "MaterialID": "501",
        "MaterialPositionID": "601",
        "Quantity": 32767
    },
    {
        "JobID": "302",
        "MaterialID": "502",
        "MaterialPositionID": "602",
        "Quantity": 0
    }
]
'''

Sample test data for file MaterialSet.json:

''' JSON
[
    {
        "MaterialID": "501",
        "Name": "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    },
    {
        "MaterialID": "502",
        "Name": "XYZ"
    }
]
'''

Sample test data for file OrderEventsSet.json:

''' JSON
[
    {
        "Date": "2019-11-05T09:52:51.137",
        "OrderEventTypeID": "1234567890123",
        "OrderEventsID": "801",
        "OrderID": "701",
        "Text": "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    },
    {
        "Date": "2019-11-05T09:52:51.137",
        "OrderEventTypeID": "9876543210987",
        "OrderEventsID": "802",
        "OrderID": "702",
        "Text": null
    }
]
'''

Sample test data for file OrderSet.json:

''' JSON
[
    {
        "CustomerID": "201",
        "Description": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "DueDate": "2019-11-05T09:52:51.138",
        "OrderID": "701",
        "OrderStatusID": "1234567890123",
        "Title": "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    },
    {
        "CustomerID": "202",
        "Description": null,
        "DueDate": "2019-11-05T09:52:51.138",
        "OrderID": "702",
        "OrderStatusID": "9876543210987",
        "Title": "XYZ"
    }
]
'''

Sample test data for file PartSet.json:

''' JSON
[
    {
        "Name": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "PartID": "901"
    },
    {
        "Name": "XYZ",
        "PartID": "902"
    }
]
'''

Sample test data for file PartsToChangeSet.json:

''' JSON
[
    {
        "JobID": "301",
        "PartID": "901",
        "PartsToChangeID": "1001"
    },
    {
        "JobID": "302",
        "PartID": "902",
        "PartsToChangeID": "1002"
    }
]
'''

Sample test data for file StepSet.json:

''' JSON
[
    {
        "JobID": "301",
        "Name": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "StepID": "1101"
    },
    {
        "JobID": "302",
        "Name": "XYZ",
        "StepID": "1102"
    }
]
'''

Sample test data for file TaskSet.json:

''' JSON
[
    {
        "AddressID": "101",
        "FinalReportStatusID": "1234567890123",
        "MachineID": "401",
        "Notes": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "OrderID": "701",
        "ScheduledDate": "2019-11-05T09:52:51.139",
        "TaskID": "1201",
        "TaskStatusID": "1234567890123",
        "Title": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "UserID": "1501"
    },
    {
        "AddressID": "102",
        "FinalReportStatusID": "9876543210987",
        "MachineID": "402",
        "Notes": null,
        "OrderID": "702",
        "ScheduledDate": null,
        "TaskID": "1202",
        "TaskStatusID": "9876543210987",
        "Title": "XYZ",
        "UserID": null
    }
]
'''

Sample test data for file ToolPositionSet.json:

''' JSON
[
    {
        "JobID": "301",
        "Quantity": 32767,
        "ToolID": "1301",
        "ToolPositionID": "1401"
    },
    {
        "JobID": "302",
        "Quantity": 0,
        "ToolID": "1302",
        "ToolPositionID": "1402"
    }
]
'''

Sample test data for file ToolSet.json:

''' JSON
[
    {
        "Name": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "ToolID": "1301"
    },
    {
        "Name": "XYZ",
        "ToolID": "1302"
    }
]
'''

Sample test data for file UserSet.json:

''' JSON
[
    {
        "Email": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "FirstNames": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "LastNames": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "Phone": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "UserID": "1501"
    },
    {
        "Email": "XYZ",
        "FirstNames": "XYZ",
        "LastNames": "XYZ",
        "Phone": "XYZ",
        "UserID": "1502"
    }
]
'''

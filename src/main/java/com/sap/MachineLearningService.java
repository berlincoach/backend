package com.sap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * MachineLearningService
 */
public class MachineLearningService {
    private final String URL = "https://mahlwerkPredictiveMaintenance.cfapps.eu10.hana.ondemand.com/predictiveMaint";
    private final Double THRESHOLD = 0.8;

    public List<Boolean> getPredictiveMaintenanceInference(List<Float> times) throws IOException, ParseException {
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {

            HttpPost httpPost = new HttpPost(URL);

            // format the request json body
            String JSON_STRING = String.format("{\"input\": [%f, %f, %f, %f, %f, %f]}", times.toArray());

            HttpEntity stringEntity = new StringEntity(JSON_STRING, ContentType.APPLICATION_JSON);
            httpPost.setEntity(stringEntity);
            CloseableHttpResponse response = httpclient.execute(httpPost);

            // check if request was sucessfull
            if (response.getStatusLine().getStatusCode() != 200) {
                com.sap.cloud.server.odata.core.DebugConsole.error("ML Service failed " + response.getStatusLine());
                return null;
            }

            String json = EntityUtils.toString(response.getEntity(), "UTF-8");
            JSONParser parser = new JSONParser();
            Object resultObject = parser.parse(json);

            if (resultObject instanceof JSONObject) {
                JSONObject obj = (JSONObject) resultObject;
                JSONArray resultArray = (JSONArray) obj.get("result");

                // extract the floating prediciton values from the result array
                ArrayList<Double> data = new ArrayList<>(resultArray.size());
                for (Object item : resultArray) {
                    data.add(Double.parseDouble(item.toString()));
                }
                List<Boolean> result = data.stream().map(v -> v > THRESHOLD).collect(Collectors.toList());
                return result;
            }

        } catch (IOException e) {
            throw e;
        } catch (ParseException e) {
            throw e;
        }
        return null;
    }

}
package com.sap.odata.handler;

import com.sap.cloud.server.odata.*;
import java.io.IOException;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.sap.MachineLearningService;
import com.sap.cloud.server.odata.DataMethod;
import com.sap.cloud.server.odata.DataQuery;
import com.sap.cloud.server.odata.DataValue;
import com.sap.cloud.server.odata.GlobalDateTime;
import com.sap.cloud.server.odata.LongValue;
import com.sap.cloud.server.odata.ParameterList;
import com.sap.odata.proxy.Job;
import com.sap.odata.proxy.JobList;
import com.sap.odata.proxy.Part;
import com.sap.odata.proxy.PartList;
import com.sap.odata.proxy.PartsToChange;
import com.sap.odata.proxy.Task;

import org.json.simple.parser.ParseException;

public class CreateJobByPredictiveMaintenanceHandler extends com.sap.cloud.server.odata.DefaultMethodHandler {
    private com.sap.odata.MainServlet servlet;
    private com.sap.odata.proxy.OdataService service;

    public CreateJobByPredictiveMaintenanceHandler(com.sap.odata.MainServlet servlet,
            com.sap.odata.proxy.OdataService service) {
        super(servlet, service);
        this.servlet = servlet;
        this.service = service;
        allowUnused(this.servlet);
        allowUnused(this.service);
    }

    @Override
    public DataValue executeMethod(DataMethod method, ParameterList parameters) {
        Long taskID = LongValue.toNullable(parameters.getRequired("taskID"));
        com.sap.odata.proxy.JobList input = createJobByPredictiveMaintenance(taskID);
        return EntityValueList.share(input);
    }

    public com.sap.odata.proxy.JobList createJobByPredictiveMaintenance(Long taskID) {
        // Method implementation code should be placed here...
        // Query Task and the job list
        DataQuery queryForTask = new DataQuery().withKey(Task.key(taskID));
        Task task = service.getTask(queryForTask);
        service.loadEntity(task);
        service.loadProperty(Task.job, task);
        JobList jobsInTask = task.getJob();

        // load all parts that are in this machine and set the time, they were last
        // changed to one week before, since we dont know, when we last changed
        PartList parts = service.getPartSet();
        SortedMap<Long, GlobalDateTime> partsChanged = new TreeMap<>();

        for (Part part : parts) {
            partsChanged.put(part.getPartID(), GlobalDateTime.now().plusDays(-7));
        }

        // get the most recent change for the parts
        for (Job job : jobsInTask) {
            service.loadProperty(Job.partsToChange, job);

            // if job is done, set the date of the parts that have been changed to the
            // DoenDate of that job
            if (job.getJobStatusID() == 1L && job.getPartsToChange() != null) {
                service.loadProperty(PartsToChange.partID, job.getPartsToChange());
                partsChanged.put(job.getPartsToChange().getPartID(), job.getDoneDate());

            }
        }

        List<Float> timeSinceLastChange = partsChanged.values().stream().map(v -> getNumberOfDatesSince(v))
                .collect(Collectors.toList());
        List<Boolean> mlResponseList;
        try {
            mlResponseList = new MachineLearningService().getPredictiveMaintenanceInference(timeSinceLastChange);
            com.sap.cloud.server.odata.core.DebugConsole.info("MLResponse " + mlResponseList);

            JobList jobList = new JobList();

            for (int i = 0; i < mlResponseList.size(); i++) {
                if (mlResponseList.get(i)) {
                    Job j = new Job();
                    j.setDefaultValues();
                    j.setTaskID(taskID);
                    j.setTitle("Change " + parts.get(i).getName());

                    PartsToChange p = new PartsToChange();
                    p.setDefaultValues();
                    p.setPartID(i + 1); // IDs in ODATA are 1-indexed
                    j.setPartsToChange(p);

                    service.createEntity(j);

                    jobList.add(j);
                }
            }
            return jobList;
        } catch (IOException e) {
            com.sap.cloud.server.odata.core.DebugConsole.error("IOEXception " + e.toString());
            return null;
        } catch (ParseException e) {
            com.sap.cloud.server.odata.core.DebugConsole.error("ParseException " + e.toString());
            return null;
        }
    }

    private float getNumberOfDatesSince(GlobalDateTime since) {
        if (since == null) {
            return 7; // Default of one week if no part was changed
        }

        return (float) GlobalDateTime.millisBetween(since, GlobalDateTime.now()) / (1000 * 60 * 60 * 24);
    }
}

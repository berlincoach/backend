package com.sap.odata.listener;

import com.sap.cloud.server.odata.*;
import com.sap.odata.proxy.Task;
import com.sap.cloud.server.odata.http.HttpRequest;
import com.sap.cloud.server.odata.json.JsonObject;
import com.sap.cloud.server.odata.json.JsonValue;

public class TaskListener extends com.sap.cloud.server.odata.DefaultEntityListener {
    private com.sap.odata.MainServlet servlet;
    private com.sap.odata.proxy.OdataService service;

    private static String BASEURL = "https://mobile-service-c2g.cfapps.eu10.hana.ondemand.com/b3be010f-57ce-4ae5-a4d5-1a0545d22140/mobileservices/origin/hcpms/CARDS/v1/register/templated";
    private static String APIKEY = "d4f932548c9d7239b43798ff0a65e06622f918cc9d3421a069b327e4b41b597d";
    private static String ALIAS = "mahlwerk";

    public TaskListener(com.sap.odata.MainServlet servlet, com.sap.odata.proxy.OdataService service) {
        super();
        this.servlet = servlet;
        this.service = service;
    }

    @Override
    public void beforeQuery(DataQuery query) {
    }

    public void beforeSave(EntityValue entityValue) {
        com.sap.odata.proxy.Task entity = (com.sap.odata.proxy.Task) entityValue;
        // Shared code for beforeCreate / beforeUpdate.
    }

    @Override
    public void beforeCreate(EntityValue entityValue) {
        com.sap.odata.proxy.Task entity = (com.sap.odata.proxy.Task) entityValue;
        beforeSave(entity);
    }

    @Override
    public void beforeUpdate(EntityValue entityValue) {
        com.sap.odata.proxy.Task entity = (com.sap.odata.proxy.Task) entityValue;
        beforeSave(entity);
        Task oldTask = service.getTask(new DataQuery().withKey(Task.key(entity.getTaskID())));// TODO move to
                                                                                              // TaskHandler
        service.loadProperty(Task.taskStatusID, oldTask);

        // prevent setting another user on the task
        if (oldTask.getUserID() != null && oldTask.getUserID() != entity.getUserID()) {
            throw DataServiceException.validationError("User has been already set.");
        }

        if (entity.getTaskStatusID() == 1 && oldTask != null && oldTask.getTaskStatusID() != 1) { // taskStatus == 1 is
                                                                                                  // scheduled
            String userID = servlet.currentUserIfAuthenticated();
            userID = "0";// TODO: remove after debugging without user
            if (userID != null) {
                entity.setUserID(Long.decode(userID));

            }

        }
    }

    @Override
    public void beforeDelete(EntityValue entityValue) {
        com.sap.odata.proxy.Task entity = (com.sap.odata.proxy.Task) entityValue;
    }

    public void afterSave(EntityValue entityValue) {
        // Shared code for afterCreate / afterUpdate.
        com.sap.odata.proxy.Task entity = (com.sap.odata.proxy.Task) entityValue;
    }

    @Override
    public void afterCreate(EntityValue entityValue) {
        com.sap.odata.proxy.Task entity = (com.sap.odata.proxy.Task) entityValue;
        afterSave(entity);
    }

    @Override
    public void afterUpdate(EntityValue entityValue) {
        com.sap.odata.proxy.Task entity = (com.sap.odata.proxy.Task) entityValue;
        afterSave(entity);

        if (entity.getTaskStatusID() == 3) {
            // get order ID
            Long orderId = entity.getOrderID();
            com.sap.cloud.server.odata.core.DebugConsole.info("ORDER ID " + orderId);

            // Send confirmation card to customer
            JsonObject requestBody = new JsonObject();
            JsonObject parameterBody = new JsonObject();

            String cardTemplateName = "MahlwerkServiceTechnicianPush"; // If you have given your card a different name,
                                                                       // please change
            // the name here.
            com.sap.odata.proxy.Order orderEntity = service.getOrderWithKey(entity.getOrderID());
            parameterBody.set("ID1", JsonValue.fromString(orderId + ""));

            requestBody.set("method", JsonValue.fromString("REGISTER"));
            requestBody.set("username", JsonValue.fromString("enrico.kaack@sap.com"));
            requestBody.set("templateName", JsonValue.fromString(cardTemplateName));
            requestBody.set("parameters", parameterBody);

            HttpRequest request = new HttpRequest();
            request.open("POST", TaskListener.BASEURL);

            request.setRequestHeader("Content-Type", "application/json");
            request.setUsername(TaskListener.ALIAS);
            request.setPassword(TaskListener.APIKEY);
            request.setRequestText(requestBody.toString());

            request.send();

            String status = request.getResponseText();
            com.sap.cloud.server.odata.core.DebugConsole.info("Card Response " + status);

            request.close();

        }
    }

    @Override
    public void afterDelete(EntityValue entityValue) {
        com.sap.odata.proxy.Task entity = (com.sap.odata.proxy.Task) entityValue;
    }
}
